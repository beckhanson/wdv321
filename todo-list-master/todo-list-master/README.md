# To Do List
Created for the purpose of teaching React to an Advanced JavaScript class at DMACC.

## Getting Started

1. In the terminal, go to the directory that you have cloned this repo in. Make sure you are in the same level that you see the `package.json`
2. If this is your first time running the app, run the command `npm install` or `yarn install`
3. Once your dependencies are installed, run the command `npm start` or `yarn start`

Your todo list should open in your default browser!

Any changes you make to the app will automatically refresh the page and surface.

## HTML example
A mirror of this React project can be found in the directory `oldSchool`.  The functionality of the `oldSchool` app is written in jQuery.

